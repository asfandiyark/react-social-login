import React from 'react'
import ReactDOM from 'react-dom'

import Demo from './containers/demo'
import { card as cardStyle, hr as hrStyle } from './utils/styles'


ReactDOM.render(
  <div style={{ ...cardStyle, padding: '1.5rem 2rem', textAlign: 'center' }}>
    <strong>GraphQL</strong>
    <hr style={hrStyle} />
    <Demo />
  </div>,
  document.getElementById('app')
)
